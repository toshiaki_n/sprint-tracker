require 'date'
require 'time'
require 'faraday'
require 'faraday_middleware'
require 'trello'
require 'gruff'
require 'pp'
require 'google/api_client'

namespace :statistics do

  # TrelloAPIアクセス用設定
  Trello.configure do |config|
    config.developer_public_key = ENV["TRELLO_API_KEY"]
    config.member_token         = ENV["TRELLO_TOKEN"]
  end

  LEVELOP_BOARD_ID = "he407g1p"
  SOLVED_LIST_ID   = "53fb683bd9b08fa725357179"
  DONE_LIST_ID     = "53d725ae73160b6a6369df7d"

  REGEXP_ESTIMATION_AND_WORK = /\((\d+\.?\d*)\).*\[(\d+\.?\d*)\]/m # 見積り時間、実作業時間を取得

  task :summarize_trello do
    board = Trello::Board.find(LEVELOP_BOARD_ID) 

    velocities      = {} # 全体の作業進捗用
    finished_map    = {} # 各日の作業進捗用
    member_name_map = {} # { member_id: name, ... }

    list_name_map = {}
    list_name_map['53fb683bd9b08fa725357179'] = 'Solved'
    list_name_map['53d725ae73160b6a6369df7d'] = 'Done' 

    first_day_of_this_week = Date.today - Date.today.wday

    board.members.each do |m|
      member_name_map[m.id] = m.attributes[:username]

      # 人別ベロシティ初期化
      velocities[m.attributes[:id]] = {}
      velocities[m.attributes[:id]][:spended]        = 0
      velocities[m.attributes[:id]][:finished]       = 0
      velocities[m.attributes[:id]][:finished_task]  = 0
      velocities[m.attributes[:id]][:estimated]      = 0
      velocities[m.attributes[:id]][:estimated_task] = 0

      finished_map[m.attributes[:id]] = {}
      finished_map[m.attributes[:id]].default = 0
      (0..6).each do |i|
        finished_map[m.attributes[:id]][first_day_of_this_week + i] = 0
      end # i
    end # m

    # 誰も割り当たっていないタスクの計算用
    member_name_map['someone'] = 'someone'
    velocities['someone'] = {}
    velocities['someone'][:spended]        = 0
    velocities['someone'][:finished]       = 0
    velocities['someone'][:finished_task]  = 0
    velocities['someone'][:estimated]      = 0
    velocities['someone'][:estimated_task] = 0

    # サマリ計算処理
    spended_total = finished_total = estimated_total = estimated_task_total = finished_task_total = 0

    # 見積作業時間計算(Doneリスト以左が対象)
    board.lists.each do |l|
      l.cards.each do |c|
        c.attributes[:name] =~ /\((\d+)\)/

        estimated  = $1.to_f

        if c.attributes[:member_ids].present?
          c.attributes[:member_ids].each do |id|
            velocities[id][:estimated]      += estimated
            velocities[id][:estimated_task] += 1
          end # id
        else
          velocities['someone'][:estimated]  += estimated
          velocities['someone'][:estimated_task] += 1
        end

        estimated_total      += estimated
        estimated_task_total += 1
      end # c
      
      break if l.id == DONE_LIST_ID
    end # l

    # 実働時間、完了分計算
    [SOLVED_LIST_ID, DONE_LIST_ID].each do |list_id|
      Trello::List.find(list_id).cards.each do |c|
        # DoneになったタイミングよりSolvedになったタイミングを優先して完了日を取得
        finished_at  = nil
        finished_at2 = nil

        c.actions.each { |a|
          case a.attributes[:type]
          when 'createCard'
            list_after = a.attributes[:data]["list"] # Card作成時の配置場所取得
          when 'updateCard'
            list_after = a.attributes[:data]["listAfter"] # Card移動の配置場所取得
          end

          next unless list_after  # Cardの移動で作成でもない or list_afterがない場合は作業完了ではない

          if list_after["name"] == "Solved" # Solvedを見つけた場合はその時点で完了日とする
            finished_at = a.date.localtime.to_date
            break
          elsif list_after["name"] == "Done" # Doneを見つけた場合は引き続きSolvedを探す
            finished_at2 = a.date.localtime.to_date
          end # if a.attributes[:data][:listAfter] == "Solved"
        } # a

        finished_at ||= finished_at2 # SolvedがなければDoneのdateを完了日とする

        c.attributes[:name] =~ REGEXP_ESTIMATION_AND_WORK

        finished = $1.to_f
        spended  = $2.to_f

        if c.attributes[:member_ids].present?
          c.attributes[:member_ids].each do |id|
            velocities[id][:spended]       += spended
            velocities[id][:finished]      += finished
            velocities[id][:finished_task] += 1
            finished_map[id][finished_at]  += finished
          end # m
        end

        spended_total       += spended
        finished_total      += finished
        finished_task_total += 1

      end # s
    end # list_id

    # 出力内容調整
    s = "現在(#{DateTime.now()})の進捗は下記でござる\n\n```"
    s += "#{"%-17s" % ''} | worked | finished(est) | estimated | last | *[] means number of tasks\n"

    # 各人の進捗出力
    velocities.each do |key, value|
      s += "#{"%-17s" % member_name_map[key]}"
      s += " | #{"%6.1f" % value[:spended]}"
      s += " | #{"%9.1f" % value[:finished]}[#{"%2d" % value[:finished_task]}]"
      s += " | #{"%5.1f" % value[:estimated]}[#{"%2d" % value[:estimated_task]}]"
      s += " | #{"%4.1f" % (value[:estimated] - value[:finished])}[#{"%2d" % (value[:estimated_task] - value[:finished_task])}]\n"
    end # key, value

    # 全体のの進捗出力
    s += "#{"%-17s" % 'all'}"
    s += " | #{"%6.1f" % spended_total}"
    s += " | #{"%9.1f" % finished_total}[#{"%2d" % finished_task_total}]"
    s += " | #{"%5.1f" % estimated_total}[#{"%2d" % finished_task_total}]"
    s += " | #{"%4.1f" % (estimated_total - finished_total)}[#{"%2d" % (estimated_task_total - finished_task_total)}]```"

    # Slackへ送信
    sl = Slack.new
    sl.send_message(s)

    # グラフ作成
    g = Gruff::Line.new 500
    g.title = "Burnup Chart" 
    g.maximum_value = 20
    g.minimum_value = 0

    g.theme_keynote

    finished_map.keys.each_with_index do |m|
      sum = 0
      array = []
      (0..6).each do |i|
        sum += finished_map[m][first_day_of_this_week+i]
        array << (Date.today >= (first_day_of_this_week + i) ? sum : nil)
      end

      array << nil # グラフがGoalとつながらないようにするため
      array << velocities[m][:estimated]
      g.data(member_name_map[m], array)
    end

    labels = {}

    (0..6).each do |i|
      labels[i] = (first_day_of_this_week + i).strftime("%m/%d\n(%a)")
    end
    labels[7] = nil
    labels[8] = "Goal"

    g.labels = labels

    sl = Slack.new
    g.write("#{Rails.root}/public/burnup_chart.png")
    res =  sl.send_file('velocity chart', "#{Rails.root}/public/burnup_chart.png")
    sl.send_message(res.env.body['file']['url'])
  end # task :summarize_trello

  task :summarize_ga do
    service_account_email = '833322407979-3e1it0pfa87a05l8q5ojq57hrae82nl7@developer.gserviceaccount.com' # Email of service account
    key_file = "#{Rails.root}/config/certifications/804f89717256ffe2b9f8e81fedf506de43991697-privatekey.p12"
    key_secret = 'notasecret' # Password to unlock private key
    profileID = '86782165' # Analytics profile ID.

    client = Google::APIClient.new(
      :application_name => 'TestProject',
      :application_version => '0.0.1'
    )

    # Load our credentials for the service account
    key = Google::APIClient::KeyUtils.load_from_pkcs12(key_file, key_secret)
    client.authorization = Signet::OAuth2::Client.new(
      :token_credential_uri => 'https://accounts.google.com/o/oauth2/token',
      :audience => 'https://accounts.google.com/o/oauth2/token',
      :scope => 'https://www.googleapis.com/auth/analytics.readonly',
      :issuer => service_account_email,
      :signing_key => key)

    # Request a token for our service account
    client.authorization.fetch_access_token!

    analytics = client.discovered_api('analytics','v3')

    startDate = DateTime.now.prev_day
    endDate = DateTime.now.prev_day

    visitCount = client.execute(:api_method => analytics.data.ga.get, :parameters => { 
      'ids' => "ga:" + profileID, 
      'start-date' => startDate.strftime("%Y-%m-%d"),
      'end-date' => endDate.strftime("%Y-%m-%d"),
      'dimensions' => "ga:day",
      'metrics' => "ga:sessions, ga:users, ga:pageviews, ga:pageviewsPerSession, ga:avgSessionDuration, ga:exitRate, ga:percentNewSessions, ga:organicSearches",
      'sort' => "ga:day" 
    })

    result = visitCount.data.totalsForAllResults

    s = "#{startDate.strftime("%Y-%m-%d")}のアクセス状況\n"
    s << "```"
    s << "セッション：　　　　#{result["ga:sessions"]}\n"
    s << "ユーザ：　　　　　　#{result["ga:users"]}\n"
    s << "検索集客数：　　　　#{result["ga:organicSearches"]}\n"
    s << "ページビュー：　　　#{result["ga:pageviews"]}\n"
    s << "ページ／セッション：#{"%.2f" % result["ga:pageviewsPerSession"]}\n"
    s << "平均セッション時間：#{result["ga:avgSessionDuration"].to_i}秒\n"
    s << "直帰率：　　　　　　#{"%.2f" % result["ga:exitRate"]}%\n"
    s << "新規セッション率：　#{"%.2f" % result["ga:percentNewSessions"]}%```\n"
    s << "詳細はこちらから：https://www.google.com/analytics/web/?#report/visitors-overview/a6802666w83770272p86782165/%3F_u.date00%3D#{startDate.strftime("%Y%m%d")}%26_u.date01%3D#{endDate.strftime("%Y%m%d")}%26overview-graphOptions.primaryConcept%3Danalytics.totalVisitors/"

    sl = Slack.new
    sl.send_message(s)

    visitCount.data.totalsForAllResults
    print visitCount.data.column_headers.map { |c|
      pp c.name  
    }.join("\t")

    visitCount.data.rows.each do |r|
      print r.join("\t"), "\n"
    end
  end 

  class Slack
    def initialize
      @conn = Faraday.new(url: 'https://slack.com') do |faraday|
        faraday.response :json, :content_type => /\bjson$/
        faraday.request :multipart
        faraday.request :url_encoded
        faraday.adapter Faraday.default_adapter
      end
    end

    def send_message(message)
      channel = (ENV['RAILS_ENV'] == 'production') ? '#11-notifications' : '#test_tn'
      post '/api/chat.postMessage', {
        token:    ENV["SLACK_API_KEY"],
        channel:  channel, # ENV["SLACK_CHANNEL"],
        username: 'leveloop bot',
        text:     message
      }
    end

    def send_file(filename, path)
      channel = (ENV['RAILS_ENV'] == 'production') ? '11-notifications' : 'TEST_TN'
      post '/api/files.upload', {
        token:    ENV["SLACK_API_KEY"],
        channels:  'test, test_tn', # ENV["SLACK_CHANNEL"],
        file:  Faraday::UploadIO.new(path, 'png'),
        filetype: 'png'
      }
    end

    private

    def post(url, params=nil)
      @conn.post do |req|
        req.body = params
        req.url url
      end
    end

  end
end # namespace :statistics
